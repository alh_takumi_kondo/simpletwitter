package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection, Integer id, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //参照
            sql.append("SELECT ");
            sql.append("    comments.id as id, ");
            sql.append("    comments.text as text, ");
            sql.append("    comments.user_id as user_id, ");
            sql.append("    comments.message_id as message_id, ");
            sql.append("    comments.created_date as created_date, ");
            sql.append("    users.account as account, ");
            sql.append("    users.name as name ");

            //テーブル名
            sql.append("FROM comments ");
            //テーブル名
            sql.append("INNER JOIN users ");
            //結合条件
            sql.append("ON comments.user_id = users.id ");

            if(id != null) {
            	sql.append("WHERE comments.user_id = ? ");
            }
            //降順にソート
            sql.append("ORDER BY created_date ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            if(id != null) {
            	ps.setInt(1, id);
            }

            ResultSet rs = ps.executeQuery();

            List<UserComment> comments = toUserComment(rs);
            return comments;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserComment(ResultSet rs) throws SQLException {

        List<UserComment> comments = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                UserComment comment = new UserComment();
                comment.setId(rs.getInt("id"));
                comment.setText(rs.getString("text"));
                comment.setUserId(rs.getInt("user_id"));
                comment.setMessageId(rs.getInt("message_id"));
                comment.setCreatedDate(rs.getTimestamp("created_date"));
                comment.setAccount(rs.getString("account"));
                comment.setName(rs.getString("name"));

                comments.add(comment);
            }
            return comments;
        } finally {
            close(rs);
        }
    }

}
