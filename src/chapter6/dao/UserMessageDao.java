package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> select(Connection connection, Integer id, int num, String startTime, String endTime) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //参照
            sql.append("SELECT ");
            //messages.idをidで取得
            sql.append("    messages.id as id, ");
            sql.append("    messages.text as text, ");
            sql.append("    messages.user_id as user_id, ");
            sql.append("    users.account as account, ");
            sql.append("    users.name as name, ");
            sql.append("    messages.created_date as created_date ");
            //テーブル名
            sql.append("FROM messages ");
            //テーブル名
            sql.append("INNER JOIN users ");
            //結合条件
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date ");
            sql.append("BETWEEN ? ");
            sql.append("AND ? ");

            if(id != null) {
            	sql.append("AND messages.user_id = ? ");
            }
            //降順にソート
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, startTime);
            ps.setString(2, endTime);

            if(id != null) {
            	ps.setInt(3, id);
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> messages = toUserMessages(rs);
            return messages;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));
                message.setText(rs.getString("text"));
                message.setUserId(rs.getInt("user_id"));
                message.setAccount(rs.getString("account"));
                message.setName(rs.getString("name"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}
