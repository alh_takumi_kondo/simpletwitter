package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void editInsert(Message message) {
    	Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().editInsert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String start, String end) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = null;
            if(!StringUtils.isBlank(userId)) {
                id = Integer.parseInt(userId);
            }

            String startTime = null;
            String endTime = null;

            if(!StringUtils.isEmpty(start)) {
            	startTime = start + " 00:00:00";
            } else {
            	startTime = "2020-01-01 00:00:00";
            }

            if(!StringUtils.isEmpty(end)) {
            	endTime = end + " 23:59:59";
            } else {
            	Calendar cl = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                endTime = sdf.format(cl.getTime());
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startTime, endTime);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message editSelect(String messageId) {
    	Connection connection = null;
        try {
            connection = getConnection();

            Message message = new MessageDao().editSelect(connection, messageId);
            commit(connection);

            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(String messageId) {
    	Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
