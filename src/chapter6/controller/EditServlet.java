package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		String PARAMETER_IS_INVALID = "不正なパラメータが入力されました";
		String messageId = request.getParameter("messageId");
		HttpSession session = request.getSession();

		if(messageId.matches("^[0-9]+$") && !messageId.isEmpty()) {
			Message editMessages = new MessageService().editSelect(messageId);
			String editMessageText = editMessages.getText();

			if(!StringUtils.isEmpty(editMessageText)) {
				request.setAttribute("editMessageText", editMessageText);
		        request.setAttribute("editMessageId", messageId);
		        request.getRequestDispatcher("edit.jsp").forward(request, response);
		        return;
			}
		}

		session.setAttribute("errorMessages", PARAMETER_IS_INVALID);
		response.sendRedirect("./");
        return;
	}

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();
        HttpSession session = request.getSession();
        String text = request.getParameter("text");
        String editMessageId = request.getParameter("messageId");

        if (!isValid(text, errorMessages)) {
        	session.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("editMessageText", text);
        	request.setAttribute("editMessageId", editMessageId);
        	request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }

        Message message = new Message();
        int intEditMessageId = Integer.parseInt(editMessageId);
        message.setText(text);
        message.setId(intEditMessageId);

        new MessageService().editInsert(message);
        response.sendRedirect("./");
    }

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
